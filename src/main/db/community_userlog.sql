-- MySQL dump 10.13  Distrib 5.6.23, for Linux (x86_64)
--
-- Host: localhost    Database: community_userlog
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_open_log`
--

drop database if exists `community_userlog`;

create database `community_userlog`;

use `community_userlog`;

DROP TABLE IF EXISTS `user_open_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_open_log` (
  `id` varchar(32) NOT NULL,
  `area` varchar(255) NOT NULL,
  `area_id` varchar(32) NOT NULL,
  `build_id` varchar(32) NOT NULL,
  `build_name` varchar(255) NOT NULL,
  `build_num` varchar(255) NOT NULL,
  `community_id` varchar(32) NOT NULL,
  `community_name` varchar(255) NOT NULL,
  `feedback` smallint(6) DEFAULT NULL,
  `feedback_time` bigint(20) DEFAULT NULL,
  `indexs` smallint(6) DEFAULT NULL,
  `lock_type` smallint(6) NOT NULL,
  `log_time` bigint(20) NOT NULL,
  `mobile_sys` varchar(255) NOT NULL,
  `mobile_type` varchar(255) NOT NULL,
  `open_time` bigint(20) NOT NULL,
  `open_type` longtext NOT NULL,
  `user_id` varchar(32) NOT NULL,
  `user_uuid` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_open_log`
--

LOCK TABLES `user_open_log` WRITE;
/*!40000 ALTER TABLE `user_open_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_open_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_opt_log`
--

DROP TABLE IF EXISTS `user_opt_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_opt_log` (
  `id` varchar(32) NOT NULL,
  `area` varchar(255) DEFAULT NULL,
  `area_id` varchar(32) DEFAULT NULL,
  `build_id` varchar(32) NOT NULL,
  `build_name` varchar(255) NOT NULL,
  `build_num` varchar(255) NOT NULL,
  `cell_id` varchar(32) NOT NULL,
  `cell_name` varchar(255) NOT NULL,
  `cell_num` varchar(255) NOT NULL,
  `community_id` varchar(32) NOT NULL,
  `community_name` varchar(255) NOT NULL,
  `device_type` smallint(6) DEFAULT NULL,
  `log_time` bigint(20) NOT NULL,
  `mobile_sys` varchar(255) NOT NULL,
  `mobile_type` varchar(255) NOT NULL,
  `opt_time` bigint(20) NOT NULL,
  `optmsg` longtext NOT NULL,
  `user_id` varchar(32) NOT NULL,
  `user_uuid` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_opt_log`
--

LOCK TABLES `user_opt_log` WRITE;
/*!40000 ALTER TABLE `user_opt_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_opt_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_set_log`
--

DROP TABLE IF EXISTS `user_set_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_set_log` (
  `id` varchar(32) NOT NULL,
  `log_time` bigint(20) NOT NULL,
  `mobile_sys` varchar(255) NOT NULL,
  `mobile_type` varchar(255) NOT NULL,
  `set_time` bigint(20) NOT NULL,
  `set_type` smallint(6) NOT NULL,
  `setmsg` longtext NOT NULL,
  `user_id` varchar(32) NOT NULL,
  `user_uuid` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_set_log`
--

LOCK TABLES `user_set_log` WRITE;
/*!40000 ALTER TABLE `user_set_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_set_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-09 17:07:20
