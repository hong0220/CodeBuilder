package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.@{EntityModel};
import com.zzwtec.community.model.@{EntityModel}M;
import com.zzwtec.community.model.@{EntityModel}MPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.@{EntityModel}ServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

<?
var date = date();
?>
/**
 * @{EntityModel}控制器
 * @author yangtonggan
 * @date  @{date,"yyyy-MM-dd"}
 * 
 */
public class @{EntityModel}Controller extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_@{ENTITY_MODEL})
	public void list(){				
		//@{EntityModel}ServicePrx prx = IceServiceUtil.getService(@{EntityModel}ServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/@{entity_model}_list.html");
	}
	
	/**
	 * 添加@{EntityModel}
	 */
	@ActionKey(UrlConstants.URL_@{ENTITY_MODEL}_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加@{EntityModel}成功");;
		try{
			//@{EntityModel}ServicePrx prx = IceServiceUtil.getService(@{EntityModel}ServicePrx.class);
			@{EntityModel} model = getBean(@{EntityModel}.class);		
			//prx.add@{EntityModel}(model);				
		}catch(Exception e){
			repJson = new DataObject("添加@{EntityModel}失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除@{EntityModel}
	 */
	@ActionKey(UrlConstants.URL_@{ENTITY_MODEL}_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除@{EntityModel}成功");
		try{
			String ids = getPara("ids");		
			//@{EntityModel}ServicePrx prx = IceServiceUtil.getService(@{EntityModel}ServicePrx.class);
			//prx.del@{EntityModel}ByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除@{EntityModel}失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_@{ENTITY_MODEL}_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		@{EntityModel} entity = new @{EntityModel}();	
		setAttr("@{entityModel}", entity);
		render("/system/view/@{entity_model}_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_@{ENTITY_MODEL}_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//@{EntityModel}ServicePrx prx = IceServiceUtil.getService(@{EntityModel}ServicePrx.class);
			//@{EntityModel}M modele = prx.inspect@{EntityModel}(id);
			@{EntityModel} entity = new @{EntityModel}();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("@{entityModel}", entity);			
			render("/system/view/@{entity_model}_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取@{EntityModel}失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改@{EntityModel}
	 */
	@ActionKey(UrlConstants.URL_@{ENTITY_MODEL}_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新@{EntityModel}成功");
		try{
			//@{EntityModel}ServicePrx prx = IceServiceUtil.getService(@{EntityModel}ServicePrx.class);
			@{EntityModel} model = getBean(@{EntityModel}.class);
			//prx.alter@{EntityModel}(model);
		}catch(Exception e){
			repJson = new DataObject("更新@{EntityModel}失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(@{EntityModel}ServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//@{EntityModel}MPage viewModel = prx.get@{EntityModel}List(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		@{EntityModel}[] aray = new @{EntityModel}[12];		
		for(int i=0;i<12;i++){
			aray[i] = new @{EntityModel}();
			aray[i].id = "id-"+i;			
		}	
		List<@{EntityModel}> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
