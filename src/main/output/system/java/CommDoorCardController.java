package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.CommDoorCard;
import com.zzwtec.community.model.CommDoorCardM;
import com.zzwtec.community.model.CommDoorCardMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.CommDoorCardServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * CommDoorCard控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class CommDoorCardController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_COMM_DOOR_CARD)
	public void list(){				
		//CommDoorCardServicePrx prx = IceServiceUtil.getService(CommDoorCardServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/comm_door_card_list.html");
	}
	
	/**
	 * 添加CommDoorCard
	 */
	@ActionKey(UrlConstants.URL_COMM_DOOR_CARD_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加CommDoorCard成功");;
		try{
			//CommDoorCardServicePrx prx = IceServiceUtil.getService(CommDoorCardServicePrx.class);
			CommDoorCard model = getBean(CommDoorCard.class);		
			//prx.addCommDoorCard(model);				
		}catch(Exception e){
			repJson = new DataObject("添加CommDoorCard失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除CommDoorCard
	 */
	@ActionKey(UrlConstants.URL_COMM_DOOR_CARD_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除CommDoorCard成功");
		try{
			String ids = getPara("ids");		
			//CommDoorCardServicePrx prx = IceServiceUtil.getService(CommDoorCardServicePrx.class);
			//prx.delCommDoorCardByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除CommDoorCard失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_COMM_DOOR_CARD_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		CommDoorCard entity = new CommDoorCard();	
		setAttr("commDoorCard", entity);
		render("/system/view/comm_door_card_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_COMM_DOOR_CARD_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//CommDoorCardServicePrx prx = IceServiceUtil.getService(CommDoorCardServicePrx.class);
			//CommDoorCardM modele = prx.inspectCommDoorCard(id);
			CommDoorCard entity = new CommDoorCard();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("commDoorCard", entity);			
			render("/system/view/comm_door_card_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取CommDoorCard失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改CommDoorCard
	 */
	@ActionKey(UrlConstants.URL_COMM_DOOR_CARD_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新CommDoorCard成功");
		try{
			//CommDoorCardServicePrx prx = IceServiceUtil.getService(CommDoorCardServicePrx.class);
			CommDoorCard model = getBean(CommDoorCard.class);
			//prx.alterCommDoorCard(model);
		}catch(Exception e){
			repJson = new DataObject("更新CommDoorCard失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(CommDoorCardServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//CommDoorCardMPage viewModel = prx.getCommDoorCardList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		CommDoorCard[] aray = new CommDoorCard[12];		
		for(int i=0;i<12;i++){
			aray[i] = new CommDoorCard();
			aray[i].id = "id-"+i;			
		}	
		List<CommDoorCard> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
