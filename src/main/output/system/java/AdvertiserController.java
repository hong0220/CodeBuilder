package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Advertiser;
import com.zzwtec.community.model.AdvertiserM;
import com.zzwtec.community.model.AdvertiserMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AdvertiserServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Advertiser控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AdvertiserController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_ADVERTISER)
	public void list(){				
		//AdvertiserServicePrx prx = IceServiceUtil.getService(AdvertiserServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/advertiser_list.html");
	}
	
	/**
	 * 添加Advertiser
	 */
	@ActionKey(UrlConstants.URL_ADVERTISER_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Advertiser成功");;
		try{
			//AdvertiserServicePrx prx = IceServiceUtil.getService(AdvertiserServicePrx.class);
			Advertiser model = getBean(Advertiser.class);		
			//prx.addAdvertiser(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Advertiser失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Advertiser
	 */
	@ActionKey(UrlConstants.URL_ADVERTISER_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Advertiser成功");
		try{
			String ids = getPara("ids");		
			//AdvertiserServicePrx prx = IceServiceUtil.getService(AdvertiserServicePrx.class);
			//prx.delAdvertiserByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Advertiser失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_ADVERTISER_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Advertiser entity = new Advertiser();	
		setAttr("advertiser", entity);
		render("/system/view/advertiser_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_ADVERTISER_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AdvertiserServicePrx prx = IceServiceUtil.getService(AdvertiserServicePrx.class);
			//AdvertiserM modele = prx.inspectAdvertiser(id);
			Advertiser entity = new Advertiser();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("advertiser", entity);			
			render("/system/view/advertiser_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Advertiser失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Advertiser
	 */
	@ActionKey(UrlConstants.URL_ADVERTISER_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Advertiser成功");
		try{
			//AdvertiserServicePrx prx = IceServiceUtil.getService(AdvertiserServicePrx.class);
			Advertiser model = getBean(Advertiser.class);
			//prx.alterAdvertiser(model);
		}catch(Exception e){
			repJson = new DataObject("更新Advertiser失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AdvertiserServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AdvertiserMPage viewModel = prx.getAdvertiserList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Advertiser[] aray = new Advertiser[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Advertiser();
			aray[i].id = "id-"+i;			
		}	
		List<Advertiser> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
