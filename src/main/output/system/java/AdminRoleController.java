package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.AdminRole;
import com.zzwtec.community.model.AdminRoleM;
import com.zzwtec.community.model.AdminRoleMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AdminRoleServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * AdminRole控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AdminRoleController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_ADMIN_ROLE)
	public void list(){				
		//AdminRoleServicePrx prx = IceServiceUtil.getService(AdminRoleServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/admin_role_list.html");
	}
	
	/**
	 * 添加AdminRole
	 */
	@ActionKey(UrlConstants.URL_ADMIN_ROLE_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加AdminRole成功");;
		try{
			//AdminRoleServicePrx prx = IceServiceUtil.getService(AdminRoleServicePrx.class);
			AdminRole model = getBean(AdminRole.class);		
			//prx.addAdminRole(model);				
		}catch(Exception e){
			repJson = new DataObject("添加AdminRole失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除AdminRole
	 */
	@ActionKey(UrlConstants.URL_ADMIN_ROLE_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除AdminRole成功");
		try{
			String ids = getPara("ids");		
			//AdminRoleServicePrx prx = IceServiceUtil.getService(AdminRoleServicePrx.class);
			//prx.delAdminRoleByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除AdminRole失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_ADMIN_ROLE_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		AdminRole entity = new AdminRole();	
		setAttr("adminRole", entity);
		render("/system/view/admin_role_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_ADMIN_ROLE_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AdminRoleServicePrx prx = IceServiceUtil.getService(AdminRoleServicePrx.class);
			//AdminRoleM modele = prx.inspectAdminRole(id);
			AdminRole entity = new AdminRole();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("adminRole", entity);			
			render("/system/view/admin_role_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取AdminRole失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改AdminRole
	 */
	@ActionKey(UrlConstants.URL_ADMIN_ROLE_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新AdminRole成功");
		try{
			//AdminRoleServicePrx prx = IceServiceUtil.getService(AdminRoleServicePrx.class);
			AdminRole model = getBean(AdminRole.class);
			//prx.alterAdminRole(model);
		}catch(Exception e){
			repJson = new DataObject("更新AdminRole失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AdminRoleServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AdminRoleMPage viewModel = prx.getAdminRoleList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		AdminRole[] aray = new AdminRole[12];		
		for(int i=0;i<12;i++){
			aray[i] = new AdminRole();
			aray[i].id = "id-"+i;			
		}	
		List<AdminRole> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
