package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.DoorCard;
import com.zzwtec.community.model.DoorCardM;
import com.zzwtec.community.model.DoorCardMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.DoorCardServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * DoorCard控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class DoorCardController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_DOOR_CARD)
	public void list(){				
		//DoorCardServicePrx prx = IceServiceUtil.getService(DoorCardServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/door_card_list.html");
	}
	
	/**
	 * 添加DoorCard
	 */
	@ActionKey(UrlConstants.URL_DOOR_CARD_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加DoorCard成功");;
		try{
			//DoorCardServicePrx prx = IceServiceUtil.getService(DoorCardServicePrx.class);
			DoorCard model = getBean(DoorCard.class);		
			//prx.addDoorCard(model);				
		}catch(Exception e){
			repJson = new DataObject("添加DoorCard失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除DoorCard
	 */
	@ActionKey(UrlConstants.URL_DOOR_CARD_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除DoorCard成功");
		try{
			String ids = getPara("ids");		
			//DoorCardServicePrx prx = IceServiceUtil.getService(DoorCardServicePrx.class);
			//prx.delDoorCardByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除DoorCard失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_DOOR_CARD_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		DoorCard entity = new DoorCard();	
		setAttr("doorCard", entity);
		render("/system/view/door_card_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_DOOR_CARD_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//DoorCardServicePrx prx = IceServiceUtil.getService(DoorCardServicePrx.class);
			//DoorCardM modele = prx.inspectDoorCard(id);
			DoorCard entity = new DoorCard();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("doorCard", entity);			
			render("/system/view/door_card_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取DoorCard失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改DoorCard
	 */
	@ActionKey(UrlConstants.URL_DOOR_CARD_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新DoorCard成功");
		try{
			//DoorCardServicePrx prx = IceServiceUtil.getService(DoorCardServicePrx.class);
			DoorCard model = getBean(DoorCard.class);
			//prx.alterDoorCard(model);
		}catch(Exception e){
			repJson = new DataObject("更新DoorCard失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(DoorCardServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//DoorCardMPage viewModel = prx.getDoorCardList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		DoorCard[] aray = new DoorCard[12];		
		for(int i=0;i<12;i++){
			aray[i] = new DoorCard();
			aray[i].id = "id-"+i;			
		}	
		List<DoorCard> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
