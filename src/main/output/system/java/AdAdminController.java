package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.AdAdmin;
import com.zzwtec.community.model.AdAdminM;
import com.zzwtec.community.model.AdAdminMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AdAdminServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * AdAdmin控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AdAdminController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN)
	public void list(){				
		//AdAdminServicePrx prx = IceServiceUtil.getService(AdAdminServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/ad_admin_list.html");
	}
	
	/**
	 * 添加AdAdmin
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加AdAdmin成功");;
		try{
			//AdAdminServicePrx prx = IceServiceUtil.getService(AdAdminServicePrx.class);
			AdAdmin model = getBean(AdAdmin.class);		
			//prx.addAdAdmin(model);				
		}catch(Exception e){
			repJson = new DataObject("添加AdAdmin失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除AdAdmin
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除AdAdmin成功");
		try{
			String ids = getPara("ids");		
			//AdAdminServicePrx prx = IceServiceUtil.getService(AdAdminServicePrx.class);
			//prx.delAdAdminByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除AdAdmin失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		AdAdmin entity = new AdAdmin();	
		setAttr("adAdmin", entity);
		render("/system/view/ad_admin_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AdAdminServicePrx prx = IceServiceUtil.getService(AdAdminServicePrx.class);
			//AdAdminM modele = prx.inspectAdAdmin(id);
			AdAdmin entity = new AdAdmin();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("adAdmin", entity);			
			render("/system/view/ad_admin_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取AdAdmin失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改AdAdmin
	 */
	@ActionKey(UrlConstants.URL_AD_ADMIN_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新AdAdmin成功");
		try{
			//AdAdminServicePrx prx = IceServiceUtil.getService(AdAdminServicePrx.class);
			AdAdmin model = getBean(AdAdmin.class);
			//prx.alterAdAdmin(model);
		}catch(Exception e){
			repJson = new DataObject("更新AdAdmin失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AdAdminServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AdAdminMPage viewModel = prx.getAdAdminList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		AdAdmin[] aray = new AdAdmin[12];		
		for(int i=0;i<12;i++){
			aray[i] = new AdAdmin();
			aray[i].id = "id-"+i;			
		}	
		List<AdAdmin> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
