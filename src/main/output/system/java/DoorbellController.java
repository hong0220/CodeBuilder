package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Doorbell;
import com.zzwtec.community.model.DoorbellM;
import com.zzwtec.community.model.DoorbellMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.DoorbellServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Doorbell控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class DoorbellController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_DOORBELL)
	public void list(){				
		//DoorbellServicePrx prx = IceServiceUtil.getService(DoorbellServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/doorbell_list.html");
	}
	
	/**
	 * 添加Doorbell
	 */
	@ActionKey(UrlConstants.URL_DOORBELL_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Doorbell成功");;
		try{
			//DoorbellServicePrx prx = IceServiceUtil.getService(DoorbellServicePrx.class);
			Doorbell model = getBean(Doorbell.class);		
			//prx.addDoorbell(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Doorbell失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Doorbell
	 */
	@ActionKey(UrlConstants.URL_DOORBELL_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Doorbell成功");
		try{
			String ids = getPara("ids");		
			//DoorbellServicePrx prx = IceServiceUtil.getService(DoorbellServicePrx.class);
			//prx.delDoorbellByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Doorbell失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_DOORBELL_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Doorbell entity = new Doorbell();	
		setAttr("doorbell", entity);
		render("/system/view/doorbell_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_DOORBELL_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//DoorbellServicePrx prx = IceServiceUtil.getService(DoorbellServicePrx.class);
			//DoorbellM modele = prx.inspectDoorbell(id);
			Doorbell entity = new Doorbell();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("doorbell", entity);			
			render("/system/view/doorbell_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Doorbell失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Doorbell
	 */
	@ActionKey(UrlConstants.URL_DOORBELL_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Doorbell成功");
		try{
			//DoorbellServicePrx prx = IceServiceUtil.getService(DoorbellServicePrx.class);
			Doorbell model = getBean(Doorbell.class);
			//prx.alterDoorbell(model);
		}catch(Exception e){
			repJson = new DataObject("更新Doorbell失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(DoorbellServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//DoorbellMPage viewModel = prx.getDoorbellList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Doorbell[] aray = new Doorbell[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Doorbell();
			aray[i].id = "id-"+i;			
		}	
		List<Doorbell> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
