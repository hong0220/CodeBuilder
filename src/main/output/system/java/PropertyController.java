package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Property;
import com.zzwtec.community.model.PropertyM;
import com.zzwtec.community.model.PropertyMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.PropertyServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Property控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class PropertyController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_PROPERTY)
	public void list(){				
		//PropertyServicePrx prx = IceServiceUtil.getService(PropertyServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/property_list.html");
	}
	
	/**
	 * 添加Property
	 */
	@ActionKey(UrlConstants.URL_PROPERTY_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Property成功");;
		try{
			//PropertyServicePrx prx = IceServiceUtil.getService(PropertyServicePrx.class);
			Property model = getBean(Property.class);		
			//prx.addProperty(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Property失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Property
	 */
	@ActionKey(UrlConstants.URL_PROPERTY_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Property成功");
		try{
			String ids = getPara("ids");		
			//PropertyServicePrx prx = IceServiceUtil.getService(PropertyServicePrx.class);
			//prx.delPropertyByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Property失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_PROPERTY_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Property entity = new Property();	
		setAttr("property", entity);
		render("/system/view/property_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_PROPERTY_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//PropertyServicePrx prx = IceServiceUtil.getService(PropertyServicePrx.class);
			//PropertyM modele = prx.inspectProperty(id);
			Property entity = new Property();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("property", entity);			
			render("/system/view/property_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Property失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Property
	 */
	@ActionKey(UrlConstants.URL_PROPERTY_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Property成功");
		try{
			//PropertyServicePrx prx = IceServiceUtil.getService(PropertyServicePrx.class);
			Property model = getBean(Property.class);
			//prx.alterProperty(model);
		}catch(Exception e){
			repJson = new DataObject("更新Property失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(PropertyServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//PropertyMPage viewModel = prx.getPropertyList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Property[] aray = new Property[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Property();
			aray[i].id = "id-"+i;			
		}	
		List<Property> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
