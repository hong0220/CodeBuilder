package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Worker;
import com.zzwtec.community.model.WorkerM;
import com.zzwtec.community.model.WorkerMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.WorkerServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Worker控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class WorkerController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_WORKER)
	public void list(){				
		//WorkerServicePrx prx = IceServiceUtil.getService(WorkerServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/worker_list.html");
	}
	
	/**
	 * 添加Worker
	 */
	@ActionKey(UrlConstants.URL_WORKER_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Worker成功");;
		try{
			//WorkerServicePrx prx = IceServiceUtil.getService(WorkerServicePrx.class);
			Worker model = getBean(Worker.class);		
			//prx.addWorker(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Worker失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Worker
	 */
	@ActionKey(UrlConstants.URL_WORKER_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Worker成功");
		try{
			String ids = getPara("ids");		
			//WorkerServicePrx prx = IceServiceUtil.getService(WorkerServicePrx.class);
			//prx.delWorkerByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Worker失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_WORKER_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Worker entity = new Worker();	
		setAttr("worker", entity);
		render("/system/view/worker_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_WORKER_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//WorkerServicePrx prx = IceServiceUtil.getService(WorkerServicePrx.class);
			//WorkerM modele = prx.inspectWorker(id);
			Worker entity = new Worker();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("worker", entity);			
			render("/system/view/worker_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Worker失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Worker
	 */
	@ActionKey(UrlConstants.URL_WORKER_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Worker成功");
		try{
			//WorkerServicePrx prx = IceServiceUtil.getService(WorkerServicePrx.class);
			Worker model = getBean(Worker.class);
			//prx.alterWorker(model);
		}catch(Exception e){
			repJson = new DataObject("更新Worker失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(WorkerServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//WorkerMPage viewModel = prx.getWorkerList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Worker[] aray = new Worker[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Worker();
			aray[i].id = "id-"+i;			
		}	
		List<Worker> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
