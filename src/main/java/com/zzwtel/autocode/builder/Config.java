package com.zzwtel.autocode.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.zzwtel.autocode.db.DBUtil;
import com.zzwtel.autocode.template.constants.LayoutType;
import com.zzwtel.autocode.template.model.ControllerModel;
import com.zzwtel.autocode.template.model.Property;
import com.zzwtel.autocode.template.model.Table;
import com.zzwtel.autocode.template.model.TemplateModel;
import com.zzwtel.autocode.template.model.UIModel;
import com.zzwtel.autocode.template.model.UrlModel;
import com.zzwtel.autocode.xml.LayoutModelParse;

/**
 * 配置，可以采用代码配置和装置xml文件进行配置 
 * @author yangtonggan
 * @date 2016-3-7
 */
public class Config {
	/**
	 * 该方法被BuilderFactory调用
	 * 实现数据模型与beetl模型相结合
	 * @return
	 */
	public static Map<String,TemplateModel> cfgModel(){
		Map<String,TemplateModel> viewMap = new HashMap<String,TemplateModel>();		
		//直接根据数据库代码生成ViewModel		
		List<Table> tables = DBUtil.getAllTables();
		for(Table t : tables){	
			if("user".equals(t.getName())){
				System.out.println("user==========********************************************");
			}
			UIModel vmList = new UIModel();
			List<Property> props = DBUtil.getProps(t);

			//配置列表
			vmList.setTable(t);
			vmList.setProps(props);			
			vmList.setLayout(LayoutType.LIST);		
			viewMap.put(viewKey(vmList), vmList);		
			
			
			if(LayoutModelParse.getColumns(t.getName()) == 2){
				//配置单列添加表单
				UIModel vmAddForm = new UIModel();
				vmAddForm.setTable(t);
				vmAddForm.setProps(props);
				vmAddForm.setLayout(LayoutType.TWO_COLUMN_ADD_FORM);
				viewMap.put(viewKey(vmAddForm), vmAddForm);
				//配置单列编辑表单
				UIModel vmEditForm = new UIModel();
				vmEditForm.setTable(t);
				vmEditForm.setProps(props);	
				vmEditForm.setLayout(LayoutType.TWO_COLUMN_EDIT_FORM);	
				viewMap.put(viewKey(vmEditForm), vmEditForm);
			}else if(LayoutModelParse.getColumns(t.getName()) == 1){
				//配置单列添加表单
				UIModel vmAddForm = new UIModel();
				vmAddForm.setTable(t);
				vmAddForm.setProps(props);
				vmAddForm.setLayout(LayoutType.SINGLE_COLUMN_ADD_FORM);
				viewMap.put(viewKey(vmAddForm), vmAddForm);
				//配置单列编辑表单
				UIModel vmEditForm = new UIModel();
				vmEditForm.setTable(t);
				vmEditForm.setProps(props);	
				vmEditForm.setLayout(LayoutType.SINGLE_COLUMN_EDIT_FORM);
				viewMap.put(viewKey(vmEditForm), vmEditForm);
			}			
			
			//目前ControllerModel默认生成增删改查方法			
			ControllerModel cm = new ControllerModel();
			cm.setTable(t);			
			viewMap.put(viewKey(cm), cm);
			
		}	
		
		return viewMap;
	}	
	
	/**
	 * 该方法被BuilderFactory调用
	 * 实现数据模型与beetl模型相结合
	 * @return
	 */
	
	public static List<UrlModel> cfgUrls(){
		List<UrlModel> urls = new ArrayList<UrlModel>();
		List<Table> tables = DBUtil.getAllTables();
		for(Table t : tables){
			//URL请求配置			
			urls.add(new UrlModel(t));
		}
		return urls;
	}
	
	/**
	 * viewKey命名规范：table-layout
	 * 根据view生成对应的key
	 * @param vm
	 * @return
	 */
	private static String viewKey(TemplateModel view){
		StringBuilder sb = new StringBuilder();
		sb.append(view.getTable().getName());
		sb.append("-");
		sb.append(uuid());
		return sb.toString();
	}
	
	/**
	 * 生成UUID字符串为模型key的一部分
	 */
	private static String uuid(){
		UUID uuid = UUID.randomUUID();  	  
	    return uuid.toString();
	}
	
}
