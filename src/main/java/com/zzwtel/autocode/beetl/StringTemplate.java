package com.zzwtel.autocode.beetl;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.StringTemplateResourceLoader;

/**
 * 字符串模板
 * @author yangtonggan
 * @date 20166-3-8
 *
 */
public class StringTemplate {		
	
	/**
	 * 获取文件名
	 * @param fileName
	 * @param varName
	 * @param model
	 * @return
	 */
	public static String getFileName(String fileName,String varName,String model){
		try{
			StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();
			Configuration cfg = Configuration.defaultConfiguration();
			GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
			Template t = gt.getTemplate(fileName);
			t.binding(varName, model);
			String str = t.render();			
			return str;
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		
	}
	
	public static void main(String[] args){
		String str = getFileName("/java/@{EntityModel}Controller.java","EntityModel","Cell");
		System.out.println(":"+str);
	}
}
