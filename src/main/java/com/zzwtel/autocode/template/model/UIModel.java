package com.zzwtel.autocode.template.model;

import java.util.List;

/**
 * 显示模型
 * @author yangtonggan
 * 映射如下xml文件
 * 
  <?xml version='1.0' encoding='UTF-8'?>
  <table name="account" model="account" class="com.pointercn.community.model.AccountModel">
	<property column="id" name="id" viewType="hidden" key="true"/>
	<property column="area_id" name="areaId" viewType="text" />
	<property column="area" name="area" type="text" />
	<property column="role_id" name="roleId" type="text" />
	<property column="name" name="name" type="text" />
	<property column="psw" name="psw" type="text" />
	<property column="level" name="level" type="text" />
  </table>
 * 
 */
public class UIModel extends TemplateModel{
	
	//属性列表
	private List<Property> props;
	//布局方式
	private String layout;
	
	
	
	public UIModel(){		
	}
	
	
	public List<Property> getProps() {
		return props;
	}
	public void setProps(List<Property> props) {
		this.props = props;
	}
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	
	
	
}
