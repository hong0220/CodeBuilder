package com.zzwtel.autocode.template.constants;

public enum ViewType {
	TEXT, // 默认
	PASSWORD, CHECKBOX, RADIO, SUBMIT, RESET, FILE, HIDDEN, IMAGE, BUTTON, TEL, //
	// HTML5
	EMAIL, URL, NUMBER, RANGE, DATE, MONTH, WEEK, TIME, DATETIME, DATETIME_LOCAL, SEARCH, COLOR;
}
