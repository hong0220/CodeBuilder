package com.zzwtel.autocode.client;

import com.zzwtel.autocode.builder.BuilderFactory;

/**
 * 代码生成器客户端
 * @author yangtonggan
 * @date 2016-3-11
 */
public class AutoMakeCodeClient {	
	public static void main(String[] args){
		//调用factory生成代码
		BuilderFactory factory = new BuilderFactory();
		factory.makeCode();
	}
}
